package com.firtsoft.xogo.Utils;

public class Languages {
    public static String[] languageEN = {"Spanish","English"};
    public static String[] languageES = {"Español","Inglés"};

    public static String[] MenuOptionEN = {"Places","Xochimilco","Services","News","Favorites"};
    public static String[] MenuOptionES = {"Lugares","Xochimilco","Servicios", "Novedades", "Favoritos"};

    public static String SearchEN = "Search";
    public static String SearchES = "Buscar";

    public static String FavsEN = "favorites";
    public static String FavsES = "favoritos";

    public static String[] CancelOptionEN = {"Accept", "Cancel"};
    public static String[] CancelOptionES = {"Aceptar", "Cancelar"};

    public static String bodyAdviceAlertLanguageEN = "The English language was selected for the application information";
    public static String bodyAdviceAlertLanguageES = "Se selecconó el idioma Español para la información de la aplicación";

}
