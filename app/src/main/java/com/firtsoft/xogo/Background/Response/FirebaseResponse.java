package com.firtsoft.xogo.Background.Response;

import com.google.gson.annotations.SerializedName;

public class FirebaseResponse {
    @SerializedName("estatus")
    public String estatus;

    @SerializedName("login")
    public String login;

    @SerializedName("mensaje")
    public String mensaje;
}
