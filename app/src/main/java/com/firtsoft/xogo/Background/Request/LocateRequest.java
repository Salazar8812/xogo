package com.firtsoft.xogo.Background.Request;

import com.google.gson.annotations.SerializedName;

public class LocateRequest {

    @SerializedName("idioma")
    public String idioma;

    @SerializedName("tipo_novedad")
    public String tipo_novedad;

    @SerializedName("tipo_operacion")
    public String tipo_operacion;

    @SerializedName("nivel")
    public String nivel;

    @SerializedName("cabecera_id")
    public String cabecera_id;

    @SerializedName("detalle_id")
    public String detalle_id;

    @SerializedName("servicio_id")
    public String servicio_id;

    @SerializedName("novedad_id")
    public String novedad_id;

    public LocateRequest() {
    }

    public LocateRequest(String idioma, String tipo_operacion, String nivel, String cabecera_id, String detalle_id) {
        this.idioma = idioma;
        this.tipo_operacion = tipo_operacion;
        this.nivel = nivel;
        this.cabecera_id = cabecera_id;
        this.detalle_id = detalle_id;
    }

    public LocateRequest(String idioma, String tipo_novedad, String tipo_operacion, String nivel) {
        this.idioma = idioma;
        this.tipo_novedad = tipo_novedad;
        this.tipo_operacion = tipo_operacion;
        this.nivel = nivel;
    }

    public LocateRequest(String idioma, String servicio_id) {
        this.idioma = idioma;
        this.servicio_id = servicio_id;
    }

    public LocateRequest(String idioma, String tipo_novedad, String novedad_id) {
        this.idioma = idioma;
        this.tipo_novedad = tipo_novedad;
        this.novedad_id = novedad_id;
    }
}
