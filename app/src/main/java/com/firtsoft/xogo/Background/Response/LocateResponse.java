
package com.firtsoft.xogo.Background.Response;

import com.firtsoft.xogo.Model.MessageLocate;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class LocateResponse {

    @SerializedName("estatus")
    @Expose
    public String estatus;
    @SerializedName("login")
    @Expose
    public String login;

    public String isService;

    @SerializedName("mensaje")
    @Expose
    public MessageLocate mensaje;

}
