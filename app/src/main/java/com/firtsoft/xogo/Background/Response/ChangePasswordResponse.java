package com.firtsoft.xogo.Background.Response;

import com.google.gson.annotations.SerializedName;

public class ChangePasswordResponse {

    @SerializedName("estatus")
    public String estatus;
    @SerializedName("mensaje")
    public String mensaje;
}
