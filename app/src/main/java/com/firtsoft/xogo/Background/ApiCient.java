package com.firtsoft.xogo.Background;

import android.util.Base64;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.Headers;
import okhttp3.OkHttpClient;
import okhttp3.Protocol;
import okhttp3.Request;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by charlssalazar on 21/08/17.
 */

public class ApiCient {
    private static String url = "https://www.xochimilcoturismo.com/apixogo/";
    private static String block_url = "http://kasav.gearhostpreview.com/";
    private static Retrofit retrofit = null;
    private static Retrofit retrofitBlock = null;

    public static Retrofit getCliente() {
        Gson gson = new GsonBuilder()
                .create();
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(url)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build();
        }
        return retrofit;
    }

    public static Retrofit getClienteBlock() {
        Gson gson = new GsonBuilder()
                .create();
        if (retrofitBlock == null) {
            retrofitBlock = new Retrofit.Builder()
                    .baseUrl(block_url)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build();
        }
        return retrofitBlock;
    }
}
