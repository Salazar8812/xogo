package com.firtsoft.xogo.Background.Response;

import com.firtsoft.xogo.Model.MessageLocate;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class ServiceDetailResponse {
    @SerializedName("estatus")
    @Expose
    public String estatus;
    @SerializedName("login")
    @Expose
    public String login;

    @SerializedName("mensaje")
    @Expose
    public List<MessageLocate> listMensaje = new ArrayList<>();
}
