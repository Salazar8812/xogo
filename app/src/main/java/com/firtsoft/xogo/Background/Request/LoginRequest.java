package com.firtsoft.xogo.Background.Request;

import com.google.gson.annotations.SerializedName;

public class LoginRequest {

    @SerializedName("email")
    public String email;
    @SerializedName("password")
    public String password;
    @SerializedName("tipo_login")
    public String tipo_login;

}
