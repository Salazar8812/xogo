package com.firtsoft.xogo.Background.Request;

import com.google.gson.annotations.SerializedName;

public class FirebaseRequest {
    @SerializedName("firebase")
    public String firebase;

    @SerializedName("idioma")
    public String idioma;

    public FirebaseRequest(String firebase, String idioma) {
        this.firebase = firebase;
        this.idioma = idioma;
    }
}
