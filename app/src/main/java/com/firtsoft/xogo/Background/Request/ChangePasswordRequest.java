package com.firtsoft.xogo.Background.Request;

import com.google.gson.annotations.SerializedName;

public class ChangePasswordRequest {

    @SerializedName("email")
    public String email;
}
