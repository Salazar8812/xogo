package com.firtsoft.xogo.Background.Response;

import com.firtsoft.xogo.Model.XogoApp;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class XogoBlockResponse {

    @SerializedName("xogo")
    @Expose
    public XogoApp xogo;
}
