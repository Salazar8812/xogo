package com.firtsoft.xogo.Background;

import com.firtsoft.xogo.Background.Request.FirebaseRequest;
import com.firtsoft.xogo.Background.Request.LocateRequest;
import com.firtsoft.xogo.Background.Request.LoginRequest;
import com.firtsoft.xogo.Background.Request.RegisterUserRequest;
import com.firtsoft.xogo.Background.Response.FirebaseResponse;
import com.firtsoft.xogo.Background.Response.LocateResponse;
import com.firtsoft.xogo.Background.Response.LoginResponse;
import com.firtsoft.xogo.Background.Response.RegisterUserResponse;
import com.firtsoft.xogo.Background.Response.ServiceDetailResponse;
import com.firtsoft.xogo.Background.Response.XogoBlockResponse;
import com.firtsoft.xogo.Model.XogoApp;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Created by charlssalazar on 21/08/17.
 */

public interface WebServices {


    @POST("api/auth/login")
    Call<LoginResponse> get_login(@Body LoginRequest request);

    @POST("api/auth/obtener_xogo_lugares")
    Call<LocateResponse> get_locate(@Body LocateRequest request);

    @POST("api/auth/servicios")
    Call<LocateResponse> get_service(@Body LocateRequest idioma);

    @POST("api/auth/servicios_detalle")
    Call<ServiceDetailResponse> get_service_detail(@Body LocateRequest idioma);

    @POST("api/auth/novedades_detalle")
    Call<ServiceDetailResponse> get_novelty_detail(@Body LocateRequest idioma);

    @POST("api/auth/novedades")
    Call<LocateResponse> get_novelty(@Body LocateRequest idioma);

    @POST("XogoService/blockApp.php")
    Call<XogoBlockResponse> blockApp();

    @POST("api/auth/registrar_usuario")
    Call<RegisterUserResponse> regidster_user(@Body RegisterUserRequest request);

    @POST("api/auth/token_firebase")
    Call<FirebaseResponse> firebase_register(@Body FirebaseRequest firebaseRequest);
}
