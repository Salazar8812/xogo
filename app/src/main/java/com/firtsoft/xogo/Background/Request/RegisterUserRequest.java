package com.firtsoft.xogo.Background.Request;

import com.google.gson.annotations.SerializedName;

public class RegisterUserRequest {

    @SerializedName("email")
    public String email;
    @SerializedName("pais")
    public String pais;
    @SerializedName("estado")
    public String estado;
    @SerializedName("ciudad")
    public String ciudad;
    @SerializedName("idioma")
    public String idioma;
    @SerializedName("sexo")
    public String sexo;
    @SerializedName("nombres")
    public String nombres;
    @SerializedName("apellidos")
    public String apellidos;
    @SerializedName("telefono")
    public String telefono;
    @SerializedName("foto")
    public String foto;
}
