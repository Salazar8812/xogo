package com.firtsoft.xogo.Activities;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.firtsoft.xogo.Activities.LoginActivity;
import com.firtsoft.xogo.InternalData.PrefsXogo;
import com.firtsoft.xogo.R;
import com.firtsoft.xogo.Utils.Languages;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SesionTypeActivity extends AppCompatActivity implements ConfirmChangeLenguageDialog.ChangeLanguageCallback {

    @BindView(R.id.acti_session_type_switch_spanish)
    TextView mLenguageSpanishTextView;

    @BindView(R.id.acti_session_type_switch_english)
    TextView mLenguageEnglishTextView;

    PrefsXogo prefsXogo;

    public static Intent launch(Context context){
        return new Intent(context,SesionTypeActivity.class);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sesion_type);
        ButterKnife.bind(this);
        prefsXogo = new PrefsXogo(this);
        setInitLanguage();
    }

    public void setEnglisLanguage(){
        mLenguageSpanishTextView.setText(Languages.languageEN[0]);
        mLenguageEnglishTextView.setText(Languages.languageEN[1]);
    }

    public void setSpanishLanguage(){
        mLenguageSpanishTextView.setText(Languages.languageES[0]);
        mLenguageEnglishTextView.setText(Languages.languageES[1]);
    }

    @OnClick(R.id.act_login_log_in)
    public void onClickLongin(){
        startActivity(new Intent(this, LoginActivity.class));
    }

    @OnClick(R.id.acti_session_type_switch_spanish)
    public void onClickSpanish(){
        mLenguageSpanishTextView.setBackgroundResource(R.drawable.background_btn_type_session_left_w);
        mLenguageSpanishTextView.setTextColor(getResources().getColor(R.color.green));
        mLenguageEnglishTextView.setTextColor(getResources().getColor(R.color.white));
        mLenguageEnglishTextView.setBackgroundResource(R.drawable.background_btn_type_session_right2_w);
        ConfirmChangeLenguageDialog confirmChangeLenguageDialog = new ConfirmChangeLenguageDialog();
        confirmChangeLenguageDialog.getCallback(this);
        startActivity(confirmChangeLenguageDialog.launch(this,"Spanish"));
    }

    @OnClick(R.id.acti_session_type_switch_english)
    public void onClickEnglish(){
        mLenguageEnglishTextView.setBackgroundResource(R.drawable.background_btn_type_session_right_w);
        mLenguageEnglishTextView.setTextColor(getResources().getColor(R.color.green));
        mLenguageSpanishTextView.setTextColor(getResources().getColor(R.color.white));
        mLenguageSpanishTextView.setBackgroundResource(R.drawable.background_btn_type_session_left2_w);
        ConfirmChangeLenguageDialog confirmChangeLenguageDialog = new ConfirmChangeLenguageDialog();
        confirmChangeLenguageDialog.getCallback(this);
        startActivity(confirmChangeLenguageDialog.launch(this,"English"));
    }

    @Override
    public void OnSetEnglishLanguage(String mTypeLanguage) {
        if(mTypeLanguage.equals("Spanish")){
            setSpanishLanguage();
            startActivity(new Intent(this,MainActivity.class));
        }else{
            setEnglisLanguage();
            startActivity(new Intent(this,MainActivity.class));
        }
    }

    public void setInitLanguage(){
        prefsXogo.saveData("typeLanguage","Spanish");
    }
}
