package com.firtsoft.xogo.Activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.firtsoft.xogo.Adapters.LocateAdapter;
import com.firtsoft.xogo.Adapters.LocateHolder;
import com.firtsoft.xogo.Background.ApiCient;
import com.firtsoft.xogo.Background.Request.LocateRequest;
import com.firtsoft.xogo.Background.Response.LocateResponse;
import com.firtsoft.xogo.Background.WebServices;
import com.firtsoft.xogo.InternalData.PrefsXogo;
import com.firtsoft.xogo.Model.DataLocate;
import com.firtsoft.xogo.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ListsSecondLevelActivity extends AppCompatActivity implements LocateAdapter.LocateCallback {
    @BindView(R.id.fr_locate_tittle)
    TextView fr_locate_tittle;

    @BindView(R.id.mListLocateRecyclerView)
    RecyclerView mListLocateRecyclerView;

    @BindView(R.id.mContentSearchLinearLayout)
    CardView mConteintCardView;

    @BindView(R.id.mSearchEditText)
    public EditText mSearchEditText;

    @BindView(R.id.mBannerTopImageView)
    ImageView mBannerTopLeftImageView;

    @BindView(R.id.mBannerTopImageView2)
    ImageView mBannerTopRigthImageView;

    @BindView(R.id.fr_locate_back)
    LinearLayout mImgBackImageView;

    private String mIsFather;
    private String mLevel;
    private String mCabeceraId;
    private String mDetailId;

    private LocateAdapter mLocateAdapter;
    private List<DataLocate> mListLocate = new ArrayList<>();
    private PrefsXogo mPrefsXogo;
    private String mTitle;
    private ProgressDialog pd;


    public static void launch(Activity activity, String mLevel, String mCabeceraId, String mDetailId, String mTitle, String isFather){
        Intent intent = new Intent(activity,ListsSecondLevelActivity.class);
        intent.putExtra("nivel",mLevel);
        intent.putExtra("cabecera_id",mCabeceraId);
        intent.putExtra("detalle_id",mDetailId);
        intent.putExtra("titulo",mTitle);
        intent.putExtra("isFather",isFather);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_locate);
        ButterKnife.bind(this);
        mPrefsXogo = new PrefsXogo(this);

        mIsFather = getIntent().getExtras().getString("isFather");
        mLevel = getIntent().getExtras().getString("nivel");
        mCabeceraId = getIntent().getExtras().getString("cabecera_id");
        mDetailId = getIntent().getExtras().getString("detalle_id");
        mTitle = getIntent().getExtras().getString("titulo");

        pd = new ProgressDialog(this);

        setTitle();
        configureRecycler();
        getList(mCabeceraId,mLevel,mDetailId);
        addListener();
        mBannerTopLeftImageView.setVisibility(View.GONE);
        mBannerTopRigthImageView.setVisibility(View.VISIBLE);
        mImgBackImageView.setVisibility(View.VISIBLE);
        setLanguageSearchBar();
    }

    public void setLanguage(){
        if(mPrefsXogo.getData("typeLanguage").equals("Spanish")){
            fr_locate_tittle.setText("Lugares");
        }else{
            fr_locate_tittle.setText("Places");
        }
    }

    public void setLanguageSearchBar(){
        if(mPrefsXogo.getData("typeLanguage").equals("Spanish")){
            mSearchEditText.setHint("Buscar");
        }else{
            mSearchEditText.setHint("Search");
        }
    }

    @OnClick(R.id.fr_locate_back)
    public void onClickBack(){
        finish();
    }

    private void configureRecycler(){
        mListLocateRecyclerView.setLayoutManager(new StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.VERTICAL));
        mLocateAdapter = new LocateAdapter(this);
        mListLocateRecyclerView.setAdapter(mLocateAdapter);
        mLocateAdapter.update(mListLocate);
    }

    public void setTitle(){
        fr_locate_tittle.setText(mTitle);
    }


    public void getList(String cabecera_id, String level, String detalle_id){
        if(mPrefsXogo.getData("typeLanguage").equals("Spanish")) {
            pd.setMessage("Iniciando...");
        }else{
            pd.setMessage("Loading...");

        }
        pd.show();
        final WebServices res = ApiCient.getCliente().create(WebServices.class);

        Call<LocateResponse> call = res.get_locate(generateRequest(cabecera_id,level,detalle_id));

        call.enqueue(new Callback<LocateResponse>() {
            @Override
            public void onResponse(Call<LocateResponse> call, Response<LocateResponse> response) {
                LocateResponse locateResponse = response.body();
                pd.dismiss();
                if(locateResponse.estatus.equals("ok")){
                    mListLocate = locateResponse.mensaje.data;
                    mLocateAdapter.update(locateResponse.mensaje.data);
                }
            }

            @Override
            public void onFailure(Call<LocateResponse> call, Throwable t) {
                pd.dismiss();
                Log.e("Error","Error al tratar de establecer conexion con el servidor");
            }
        });
    }

    public LocateRequest generateRequest(String cabecera_id, String level, String detalle_id){
        LocateRequest locateRequest = new LocateRequest();
        switch (mTitle){
            case "Lugares":
                locateRequest= new LocateRequest(mPrefsXogo.getData("typeLanguage").equals("English") ? "I" : "E","L",level,cabecera_id,detalle_id);
                setLanguage();
                break;
            case "Places":
                locateRequest= new LocateRequest(mPrefsXogo.getData("typeLanguage").equals("English") ? "I" : "E","L",level,cabecera_id,detalle_id);
                setLanguage();
                break;
            case "Xochimilco":
                locateRequest = new LocateRequest(mPrefsXogo.getData("typeLanguage").equals("English") ? "I" : "E","X",level,cabecera_id,detalle_id);
                break;
                default:break;
        }

        return locateRequest;
    }

    public void addListener(){
        mSearchEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String cad = "";
                cad = cad +s;
                searchInList(cad);
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }

    public void searchInList(String mParamSearch){
        List<DataLocate> mlistSearchResult = new ArrayList<>();
        for (DataLocate item: mListLocate) {
            if(item.titulo.toLowerCase().contains(mParamSearch.toLowerCase()) || item.titulo.toLowerCase().startsWith(mParamSearch.toLowerCase())){
                mlistSearchResult.add(item);
            }
        }
        mLocateAdapter.update(mlistSearchResult);
    }
    @Override
    public void OnSelectLocate(LocateHolder vh) {
        isFather(vh.getLocate());
    }

    public void isFather(DataLocate mDataLocate){
        if(mDataLocate.esPadre.equals("S")){
            ListThirdLevelActivity.launch(this,"3",mCabeceraId,mDataLocate.detalle_id,mTitle,"S");
        }else if(mDataLocate.esPadre.equals("N")){
            DetailActivity.launch(this,mDataLocate.descripcion,mDataLocate.urlFoto, "", mDataLocate.titulo, "", mDataLocate.nombre, mDataLocate,"false","");
        }
    }
}
