package com.firtsoft.xogo.Activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.firtsoft.xogo.Background.ApiCient;
import com.firtsoft.xogo.Background.Response.XogoBlockResponse;
import com.firtsoft.xogo.Background.WebServices;
import com.firtsoft.xogo.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        getBlockApp();
    }


    public void getBlockApp(){
        final WebServices res = ApiCient.getClienteBlock().create(WebServices.class);
        Call<XogoBlockResponse> call = res.blockApp();

        call.enqueue(new Callback<XogoBlockResponse>() {
            @Override
            public void onResponse(Call<XogoBlockResponse> call, Response<XogoBlockResponse> response) {
                XogoBlockResponse xogoBlock = response.body();
                if(xogoBlock.xogo.blockApp.equals("No")){
                    SplashActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                Thread.sleep(1500);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                    if (!prefs.getBoolean("firstTime", false)) {
                        startActivity(SesionTypeActivity.launch(getApplicationContext()));
                        finish();
                    } else {
                        startActivity(MainActivity.launch(getApplicationContext()));
                        finish();
                    }

                }else{

                    dialogBlock();
                }

            }

            @Override
            public void onFailure(Call<XogoBlockResponse> call, Throwable t) {
                Log.e("Error","Error al tratar de establecer conexion con el servidor");
            }
        });
    }

    public void dialogBlock(){
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(SplashActivity.this, android.R.style.Theme_Material_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(SplashActivity.this);
        }
        builder.setTitle("Block App")
                .setMessage("Licencia Bloqueada")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        SplashActivity.this.finish();
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setCancelable(false)
                .show();
    }

}
