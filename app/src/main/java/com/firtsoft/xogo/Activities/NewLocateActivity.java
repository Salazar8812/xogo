package com.firtsoft.xogo.Activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.firtsoft.xogo.R;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class NewLocateActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_locate);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.toolbar_back)
    public void onClicBack(){
        finish();
    }
}
