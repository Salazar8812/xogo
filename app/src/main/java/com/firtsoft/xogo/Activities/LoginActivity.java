package com.firtsoft.xogo.Activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.firtsoft.xogo.Background.ApiCient;
import com.firtsoft.xogo.Background.Request.LoginRequest;
import com.firtsoft.xogo.Background.Response.LoginResponse;
import com.firtsoft.xogo.Background.WebServices;
import com.firtsoft.xogo.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity implements FacebookCallback<LoginResult>, GraphRequest.GraphJSONObjectCallback {

    @BindView(R.id.act_login_user)
    EditText mUserLogEditText;
    @BindView(R.id.act_login_pass)
    EditText mPassEditText;
    @BindView(R.id.login_button_facebook)
    LoginButton mLoginFacebook;
    private String user;
    private String pass;
    private String type;
    private CallbackManager callbackManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        requestPermission();
        configureLoginFacebook();
    }

    public void configureLoginFacebook(){
        callbackManager = CallbackManager.Factory.create();
        mLoginFacebook.setReadPermissions("email", "user_birthday");
        mLoginFacebook.registerCallback(callbackManager, this);
    }

    @OnClick(R.id.act_login_init)
    public void onClickLongin(){
    }

    @OnClick(R.id.act_login_init)
    public void onClickStarSession(){
        user = mUserLogEditText.getText().toString().trim();
        pass = mPassEditText.getText().toString().trim();
        if (user.equals("") || pass.equals("")){
            Toast.makeText(this,"Es necesario llenar los campos",Toast.LENGTH_LONG).show();
        }else {
            get_login();
        }
    }

    public void get_login() {
        WebServices res = ApiCient.getCliente().create(WebServices.class);

        LoginRequest request= new LoginRequest();
        request.email = mUserLogEditText.getText().toString().trim();
        request.password =  mPassEditText.getText().toString().trim();
        request.tipo_login = "N";
        Call<LoginResponse> call = res.get_login(request);

        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                LoginResponse loginResponse = response.body();
                try {
                    if (loginResponse.estatus.equals("ok")){
                        //Log.e("#################",loginResponse.token);
                        startActivity(new Intent(LoginActivity.this, MainActivity.class));
                    } else {
                        Toast.makeText(LoginActivity.this, "Correo electrónico o contraseña incorrecta", Toast.LENGTH_LONG).show();
                    }
                }catch (NullPointerException e){
                    e.printStackTrace();
                    Toast.makeText(LoginActivity.this, "Correo electrónico o contraseña incorrecta", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                //Log.d("###################" , ""+t);
                Toast.makeText(LoginActivity.this,"Servicio temporalmente no disponible",Toast.LENGTH_LONG).show();
            }
        });
    }

    private void requestPermission() {
        if (ActivityCompat.checkSelfPermission(LoginActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(LoginActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ) {
            ActivityCompat.requestPermissions(LoginActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.ACCESS_COARSE_LOCATION}, 1); //Any number can be used    }
        }else{
            Log.d("Permisos","Permisos Otorgados");
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onSuccess(LoginResult loginResult) {
        GraphRequest.newMeRequest(loginResult.getAccessToken(), this).executeAsync();
        startActivity(new Intent(LoginActivity.this, MainActivity.class));
    }

    @Override
    public void onCancel() {

    }

    @Override
    public void onError(FacebookException error) {

    }

    @Override
    public void onCompleted(JSONObject jsonObject, GraphResponse graphResponse) {
        try {
            if (jsonObject.has("birthday"))
                Log.e("Fecha Nacimiento", jsonObject.getString("birthday"));
            if (jsonObject.has("email"))
                Log.e("Correo", jsonObject.getString("email"));
        } catch (NullPointerException | JSONException e) {
            e.printStackTrace();
        }
    }
}
