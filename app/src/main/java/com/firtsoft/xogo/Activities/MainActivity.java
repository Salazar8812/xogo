package com.firtsoft.xogo.Activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.preference.PreferenceManager;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.firtsoft.xogo.Background.ApiCient;
import com.firtsoft.xogo.Background.Request.FirebaseRequest;
import com.firtsoft.xogo.Background.Response.FirebaseResponse;
import com.firtsoft.xogo.Background.Response.XogoBlockResponse;
import com.firtsoft.xogo.Model.XogoApp;
import com.firtsoft.xogo.Background.WebServices;
import com.firtsoft.xogo.Fragments.FavoritiesFragment;
import com.firtsoft.xogo.Fragments.LocateFragment;
import com.firtsoft.xogo.Fragments.NoveltyFragment;
import com.firtsoft.xogo.Fragments.ServicesFragment;
import com.firtsoft.xogo.Fragments.XogoFragment;
import com.firtsoft.xogo.InternalData.PrefsXogo;
import com.firtsoft.xogo.R;
import com.firtsoft.xogo.Adapters.TabAdapter;
import com.firtsoft.xogo.Utils.Languages;
import com.google.firebase.iid.FirebaseInstanceId;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity  implements LocationListener {

    public static Intent launch(Context context) {
        return new Intent(context, MainActivity.class);
    }

    @BindView(R.id.act_main_menu_title1)
    public TextView title1;
    @BindView(R.id.act_main_menu_title2)
    public TextView title2;
    @BindView(R.id.act_main_menu_title3)
    public TextView title3;
    @BindView(R.id.act_main_menu_title4)
    public TextView title4;
    @BindView(R.id.act_main_menu_title5)
    public TextView title5;
    private TabAdapter adapter;
    private ViewPager viewPager;
    private TabLayout tabLayout;
    private PrefsXogo mPrefsXogo;

    protected LocationManager locationManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setupPager();
        mPrefsXogo = new PrefsXogo(this);
        setLanguageTitle();
        requestPermission();
        Log.d("Firebase", "token "+ FirebaseInstanceId.getInstance().getToken());
        registerFireBase();
    }


    @SuppressLint("MissingPermission")
    public void prepareLocation() {
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
    }

    private void setupPager(){
        viewPager = findViewById(R.id.viewPager);
        tabLayout = findViewById(R.id.tabLayout);
        adapter = new TabAdapter(getSupportFragmentManager(), this);
        adapter.addFragment(new LocateFragment(), " ");
        adapter.addFragment(new XogoFragment(), " ");
        adapter.addFragment(new ServicesFragment(), " ");
        adapter.addFragment(new NoveltyFragment(), " ");
        adapter.addFragment(new FavoritiesFragment()," ");
        viewPager.setAdapter(adapter);
        viewPager.setOffscreenPageLimit(4);
        tabLayout.setupWithViewPager(viewPager);
        setIconsTab(0);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                setIconsTab(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    private void setIconsTab(int position){
        setInitialsIconsTab();
        switch (position){
            case 0:
                title1.setTextColor(getResources().getColor(R.color.red));
                title2.setTextColor(getResources().getColor(R.color.gray));
                title3.setTextColor(getResources().getColor(R.color.gray));
                title4.setTextColor(getResources().getColor(R.color.gray));
                title5.setTextColor(getResources().getColor(R.color.gray));
                tabLayout.getTabAt(0).setIcon(R.drawable.ic_locate_on);
                break;
            case 1:
                title1.setTextColor(getResources().getColor(R.color.gray));
                title2.setTextColor(getResources().getColor(R.color.red));
                title3.setTextColor(getResources().getColor(R.color.gray));
                title4.setTextColor(getResources().getColor(R.color.gray));
                title5.setTextColor(getResources().getColor(R.color.gray));
                tabLayout.getTabAt(1).setIcon(R.drawable.ic_xochimilco_on);
                break;
            case 2:
                title1.setTextColor(getResources().getColor(R.color.gray));
                title2.setTextColor(getResources().getColor(R.color.gray));
                title3.setTextColor(getResources().getColor(R.color.red));
                title4.setTextColor(getResources().getColor(R.color.gray));
                title5.setTextColor(getResources().getColor(R.color.gray));
                tabLayout.getTabAt(2).setIcon(R.drawable.ic_services_on);
                break;
            case 3:
                title1.setTextColor(getResources().getColor(R.color.gray));
                title2.setTextColor(getResources().getColor(R.color.gray));
                title3.setTextColor(getResources().getColor(R.color.gray));
                title4.setTextColor(getResources().getColor(R.color.red));
                title5.setTextColor(getResources().getColor(R.color.gray));
                tabLayout.getTabAt(3).setIcon(R.drawable.ic_novelty_on);
                break;
            case 4:
                title1.setTextColor(getResources().getColor(R.color.gray));
                title2.setTextColor(getResources().getColor(R.color.gray));
                title3.setTextColor(getResources().getColor(R.color.gray));
                title4.setTextColor(getResources().getColor(R.color.gray));
                title5.setTextColor(getResources().getColor(R.color.red));
                tabLayout.getTabAt(4).setIcon(R.drawable.ic_favoritos_on);
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    public void setLanguageTitle(){
        if(mPrefsXogo.getData("typeLanguage").equals("Spanish")){
            title1.setText(Languages.MenuOptionES[0]);
            title2.setText(Languages.MenuOptionES[1]);
            title3.setText(Languages.MenuOptionES[2]);
            title4.setText(Languages.MenuOptionES[3]);
            title5.setText(Languages.MenuOptionES[4]);
        }else{
            title1.setText(Languages.MenuOptionEN[0]);
            title2.setText(Languages.MenuOptionEN[1]);
            title3.setText(Languages.MenuOptionEN[2]);
            title4.setText(Languages.MenuOptionEN[3]);
            title5.setText(Languages.MenuOptionEN[4]);

        }
    }

    private void setInitialsIconsTab(){
        tabLayout.getTabAt(0).setIcon(R.drawable.ic_locate_off);
        tabLayout.getTabAt(1).setIcon(R.drawable.ic_xochimilco_off);
        tabLayout.getTabAt(2).setIcon(R.drawable.ic_services_off);
        tabLayout.getTabAt(3).setIcon(R.drawable.ic_novelty_off);
        tabLayout.getTabAt(4).setIcon(R.drawable.ic_favoritos_off);
    }

    private void requestPermission() {
        if (ActivityCompat.checkSelfPermission(MainActivity.this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(MainActivity.this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ) {
            ActivityCompat.requestPermissions(MainActivity.this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION, android.Manifest.permission.WRITE_EXTERNAL_STORAGE, android.Manifest.permission.READ_EXTERNAL_STORAGE, android.Manifest.permission.ACCESS_COARSE_LOCATION}, 1); //Any number can be used    }
        }else{
            prepareLocation();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    prepareLocation();
                }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        prepareLocation();
    }

    @Override
    public void onLocationChanged(Location location) {
        mPrefsXogo.saveData("latitud" , String.valueOf(location.getLatitude()));
        mPrefsXogo.saveData("longitud" , String.valueOf(location.getLongitude()));
        Log.e("Location","Latitude:" + location.getLatitude() + ", Longitude:" + location.getLongitude());
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    public void registerFireBase(){
        final WebServices res = ApiCient.getCliente().create(WebServices.class);
        FirebaseRequest firebaseRequest = new FirebaseRequest(mPrefsXogo.getData("typeLanguage").equals("English") ? "I" : "E",FirebaseInstanceId.getInstance().getToken());

        Call<FirebaseResponse> call = res.firebase_register(firebaseRequest);

        call.enqueue(new Callback<FirebaseResponse>() {
            @Override
            public void onResponse(Call<FirebaseResponse> call, Response<FirebaseResponse> response) {
                FirebaseResponse firebaseResponse = response.body();
                if(firebaseResponse.estatus.equals("")){

                }else{

                }
            }

            @Override
            public void onFailure(Call<FirebaseResponse> call, Throwable t) {
                Log.e("Error","Error al tratar de establecer conexion con el servidor");
            }
        });
    }


}
