package com.firtsoft.xogo.Activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.firtsoft.xogo.Adapters.FavsAdapter;
import com.firtsoft.xogo.Adapters.FavsHolder;
import com.firtsoft.xogo.InternalData.PrefsXogo;
import com.firtsoft.xogo.Model.DataLocate;
import com.firtsoft.xogo.R;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class FavsActivity extends AppCompatActivity implements FavsAdapter.LocateCallback {
    @BindView(R.id.mListFavorites)
    public RecyclerView mListFavorites;

    @BindView(R.id.act_favs_back)
    public LinearLayout act_favs_back;

    @BindView(R.id.act_favs_list_alert)
    public TextView act_favs_list_alert;

    @BindView(R.id.mTitleTextView)
    public TextView mTitleTextView;

    private FavsAdapter favsAdapter;
    private PrefsXogo mPrefsXogo;

    private List<DataLocate> mListFavs = new ArrayList<>();

    public static void launch(Activity activity){
        Intent intent = new Intent(activity, FavsActivity.class);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favs);
        ButterKnife.bind(this);
        mPrefsXogo = new PrefsXogo(this);
        configureRecycler();
        setLanguage();
    }

    public void setLanguage(){
        if(mPrefsXogo.getData("typeLanguage").equals("Spanish")){
            mTitleTextView.setText("Favoritos");
        }else{
            mTitleTextView.setText("Favorites");
        }
    }

    private void configureRecycler(){
        mListFavorites.setLayoutManager(new LinearLayoutManager(this));
        favsAdapter = new FavsAdapter(this);
        mListFavorites.setAdapter(favsAdapter);
        mListFavs = retrieveList();
        if(mListFavs == null){
            mListFavs = new ArrayList<>();
        }else{
            act_favs_list_alert.setVisibility(View.GONE);
        }

        favsAdapter.update(mListFavs);
    }

    public List<DataLocate> retrieveList(){
        Gson gson = new Gson();
        String response=mPrefsXogo.getData("favs");
        List<DataLocate> lstArrayList = gson.fromJson(response,
                new TypeToken<List<DataLocate>>(){}.getType());

        return lstArrayList;
    }

    @OnClick(R.id.act_favs_back)
    public void OnBackClick(){
        onBackPressed();
    }

    @Override
    public void OnSelectLocate(FavsHolder vh) {
        DetailActivity.launch(this,vh.getLocate().descripcion,vh.getLocate().urlFoto, "", vh.getLocate().titulo, "", vh.getLocate().nombre, vh.getLocate(),"Favs","");
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
