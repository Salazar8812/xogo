package com.firtsoft.xogo.Activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.firtsoft.xogo.InternalData.PrefsXogo;
import com.firtsoft.xogo.R;
import com.firtsoft.xogo.Utils.Languages;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ConfirmChangeLenguageDialog extends AppCompatActivity {

    public static Intent launch(Context context, String typeSeller){
        Intent intent = new Intent(context,ConfirmChangeLenguageDialog.class);
        intent.putExtra("typeLenguage",typeSeller);
        return  intent;
    }
    @BindView(R.id.dialog_confirm_lenguage_type)
    TextView typeLenguageTextView;

    @BindView(R.id.dialog_confirm_lenguage_accept)
    TextView dialog_confirm_lenguage_accept;

    @BindView(R.id.dialog_confirm_lenguage_cancel)
    TextView dialog_confirm_lenguage_cancel;


    private String typeLenguage;
    private static ChangeLanguageCallback mChangeLanguageCallback;
    private PrefsXogo mPrefsXogo;

    public void getCallback(ChangeLanguageCallback mChangeLanguageCallback){
        this.mChangeLanguageCallback = mChangeLanguageCallback;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_confirm_change_lenguage);
        typeLenguage = getIntent().getStringExtra("typeLenguage");
        mPrefsXogo = new PrefsXogo(this);
        ButterKnife.bind(this);
        initLanguage();
    }

    public void initLanguage(){
        switch (typeLenguage){
            case "Spanish":
                setLanguageES();
                break;
            case "English":
                setLanguageEN();
                break;
            default:setLanguageES();
                break;
        }
    }

    public void setLanguageEN(){
        typeLenguageTextView.setText(Languages.bodyAdviceAlertLanguageEN);
        dialog_confirm_lenguage_accept.setText(Languages.CancelOptionEN[0]);
        dialog_confirm_lenguage_cancel.setText(Languages.CancelOptionEN[1]);
    }

    public void setLanguageES(){
        typeLenguageTextView.setText(Languages.bodyAdviceAlertLanguageES);
        dialog_confirm_lenguage_accept.setText(Languages.CancelOptionES[0]);
        dialog_confirm_lenguage_cancel.setText(Languages.CancelOptionES[1]);
    }

    public void changeTagLanguage(){
        switch (mPrefsXogo.getData("typeLanguage")){
            case "Spanish":
                setLanguageES();
                break;
            case "English":
                setLanguageEN();
                break;
                default:setLanguageES();
                break;
        }
    }

    @OnClick(R.id.dialog_confirm_lenguage_accept)
    public void onClickBtnAccept(){
        startActivity(new Intent(this, MainActivity.class));
        mPrefsXogo.saveData("typeLanguage",typeLenguage);
        mChangeLanguageCallback.OnSetEnglishLanguage(typeLenguage);
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean("firstTime", true);
        editor.apply();
        finish();
    }

    @OnClick(R.id.dialog_confirm_lenguage_cancel)
    public void onClickBtnCancel(){
        //mChangeLanguageCallback.OnSetEnglishLanguage(typeLenguage);
       finish();
    }

    public interface ChangeLanguageCallback{
        void OnSetEnglishLanguage(String mTypeLanguage);
    }
}
