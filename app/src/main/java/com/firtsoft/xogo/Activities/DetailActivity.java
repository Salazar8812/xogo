package com.firtsoft.xogo.Activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.firtsoft.xogo.Background.ApiCient;
import com.firtsoft.xogo.Background.Request.LocateRequest;
import com.firtsoft.xogo.Background.Response.LocateResponse;
import com.firtsoft.xogo.Background.Response.ServiceDetailResponse;
import com.firtsoft.xogo.Background.WebServices;
import com.firtsoft.xogo.InternalData.PrefsXogo;
import com.firtsoft.xogo.Model.DataLocate;
import com.firtsoft.xogo.R;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetailActivity extends AppCompatActivity {

    @BindView(R.id.act_detail_seccion_image)
    ImageView mSeccionImageView;

    @BindView(R.id.act_detail_seccion_tittle)
    TextView mSeccionTittleTextView;

    @BindView(R.id.act_detail_seccion_description)
    WebView mSeccionDescriptionTextView;

    @BindView(R.id.mFavImageView)
    ImageView mFavImageView;

    @BindView(R.id.mShareContentImageView)
    ImageView mShareContentImageView;

    @BindView(R.id.mLocationImageView)
    ImageView mLocationImageView;

    private String mDescription;
    private String mUrlFoto;
    private String mTitle;
    private String mNombre;
    private PrefsXogo mPrefsXogo;
    private DataLocate mDataLocate;
    private String isService;
    private String mTypeNovelty;
    private ProgressDialog pd;
    private String mShareLink;

    private boolean isPlaces = true;



    public static void launch(Activity activity, String mDescription, String mUrlFoto, String mDetailId, String mTitle, String isFather, String mNombre, DataLocate mDataLocate, String isService, String typeNovelty) {
        Intent intent = new Intent(activity, DetailActivity.class);
        intent.putExtra("descripcion", mDescription);
        intent.putExtra("urlFoto", mUrlFoto);
        intent.putExtra("detalle_id", mDetailId);
        intent.putExtra("titulo", mTitle);
        intent.putExtra("nombre", mNombre);
        intent.putExtra("isFather", isFather);
        intent.putExtra("item", mDataLocate);
        intent.putExtra("isServce", isService);
        intent.putExtra("typeNovelty", typeNovelty);
        activity.startActivity(intent);
    }


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        ButterKnife.bind(this);
        mDescription = getIntent().getExtras().getString("descripcion");
        mUrlFoto = getIntent().getExtras().getString("urlFoto");
        mTitle = getIntent().getExtras().getString("titulo");
        mNombre = getIntent().getExtras().getString("nombre");
        mDataLocate = (DataLocate) getIntent().getExtras().getSerializable("item");
        isService = getIntent().getExtras().getString("isServce");
        mTypeNovelty = getIntent().getExtras().getString("typeNovelty");
        pd = new ProgressDialog(this);

        if (mTitle != null) {
            mSeccionTittleTextView.setText(mTitle);
        } else {
            mSeccionTittleTextView.setText(mNombre);
        }

        mSeccionDescriptionTextView.loadData(mDescription, "text/html", "UTF-8");
        RequestOptions requestOption = new RequestOptions()
                .placeholder(R.drawable.ic_placeholder).centerCrop();
        Glide.with(this).load(mUrlFoto).apply(requestOption).into(mSeccionImageView);

        mPrefsXogo = new PrefsXogo(this);

        detectDetail();
    }


    public void detectDetail(){
        if(isService.equals("Servicios")){
            getServiceDetail();
            getShareLink("Servicios");
            isPlaces = false;

        }

        if(isService.equals("Novedades")){
            getNoveltyDetail();
            getShareLink("Novedades");
            isPlaces = false;
        }

        if(isService.equals("Favs")){
            hideFavsControls();
        }

        if(isPlaces){
            getShareLink("random");
        }

        detectLocation();
    }

    public void hideFavsControls(){
        mFavImageView.setVisibility(View.INVISIBLE);
        mShareContentImageView.setVisibility(View.INVISIBLE);
    }

    @OnClick(R.id.toolbar_back_transparent)
    public void onCLickBack() {
        finish();
    }

    @OnClick(R.id.mFavImageView)
    public void OnAddFav(){
        List<DataLocate> mListTemp = retrieveList();

        if(mListTemp == null){
            mListTemp = new ArrayList<>();
        }

        if(!isSameItem(mListTemp)) {
            mListTemp.add(mDataLocate);
            saveList(mListTemp);
        }else{
            Toast.makeText(this,"Ya lo tienes agregado a favoritos",Toast.LENGTH_LONG).show();
        }
    }

    @OnClick(R.id.mLocationImageView)
    public void OnLocation(){
        Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
                Uri.parse("http://maps.google.com/maps?saddr="+mPrefsXogo.getData("latitud")+","+mPrefsXogo.getData("longitud")+"&daddr="+mDataLocate.latitud+","+mDataLocate.longitud));
                startActivity(intent);
    }

    @OnClick(R.id.mShareContentImageView)
    public void OnShareContentClick(){
        shareContent();
    }

    private boolean isSameItem(List<DataLocate> mList){
        boolean isSame = false;
        for(DataLocate item : mList){
            if(item.detalle_id != null && mDataLocate.detalle_id != null){
                if(mDataLocate.detalle_id.equals(item.detalle_id)){
                    isSame = true ;
                    break;
                }else{
                    isSame = false;
                    break;
                }
            }else if (mDataLocate.id != null && item.id != null){
                if(mDataLocate.id.equals(item.id)){
                    isSame = true ;
                    break;
                }else{
                    isSame = false;
                    break;
                }
            }
        }
        return isSame;
    }

    public List<DataLocate> retrieveList(){
        Gson gson = new Gson();
        String response=mPrefsXogo.getData("favs");
        List<DataLocate> lstArrayList = gson.fromJson(response,
                new TypeToken<List<DataLocate>>(){}.getType());
        return lstArrayList;
    }

    public void saveList(List<DataLocate> mListDataLocate){
        Gson gson = new Gson();

        String json = gson.toJson(mListDataLocate);
        mPrefsXogo.saveData("favs",json);
        Toast.makeText(this,"Agreagado a favoritos",Toast.LENGTH_LONG).show();
    }

    public void shareContent(){
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_SUBJECT, "Xogo");
        intent.putExtra(Intent.EXTRA_TEXT, mShareLink);
        startActivity(Intent.createChooser(intent,  "Seleccione:"));
    }

    public void getServiceDetail(){
        if(mPrefsXogo.getData("typeLanguage").equals("Spanish")) {
            pd.setMessage("Iniciando...");
        }else{
            pd.setMessage("Loading...");
        }
        pd.show();
        final WebServices res = ApiCient.getCliente().create(WebServices.class);
        Call<ServiceDetailResponse> call = res.get_service_detail(new LocateRequest(mPrefsXogo.getData("typeLanguage").equals("English") ? "E":"S",mDataLocate.id));

        call.enqueue(new Callback<ServiceDetailResponse>() {
            @Override
            public void onResponse(Call<ServiceDetailResponse> call, Response<ServiceDetailResponse> response) {
                ServiceDetailResponse locateResponse = response.body();
                if(locateResponse.estatus.equals("ok")){
                    pd.dismiss();
                    mDescription = locateResponse.listMensaje.get(0).descripcion;
                    setDataService(locateResponse);
                    mSeccionDescriptionTextView.loadData(mDescription, "text/html", "UTF-8");
                    Glide.with(DetailActivity.this).load(mUrlFoto).into(mSeccionImageView);
                }
            }
            @Override
            public void onFailure(Call<ServiceDetailResponse> call, Throwable t) {
                pd.dismiss();
                Log.e("Error",t.toString());
            }
        });
    }

    public void getNoveltyDetail(){
        if(mPrefsXogo.getData("typeLanguage").equals("Spanish")) {
            pd.setMessage("Iniciando...");
        }else{
            pd.setMessage("Loading...");

        }
        pd.show();
        final WebServices res = ApiCient.getCliente().create(WebServices.class);
        Call<ServiceDetailResponse> call = res.get_novelty_detail(new LocateRequest(mPrefsXogo.getData("typeLanguage").equals("English") ? "E":"S",mTypeNovelty,mDataLocate.id));

        call.enqueue(new Callback<ServiceDetailResponse>() {
            @Override
            public void onResponse(Call<ServiceDetailResponse> call, Response<ServiceDetailResponse> response) {
                ServiceDetailResponse locateResponse = response.body();
                pd.dismiss();
                if(locateResponse.estatus.equals("ok")){
                    mDescription = locateResponse.listMensaje.get(0).descripcion;
                    setDataService(locateResponse);
                    mSeccionDescriptionTextView.loadData(mDescription, "text/html", "UTF-8");
                    Glide.with(DetailActivity.this).load(mUrlFoto).into(mSeccionImageView);
                }
            }
            @Override
            public void onFailure(Call<ServiceDetailResponse> call, Throwable t) {
                pd.dismiss();
                Log.e("Error",t.toString());
            }
        });
    }

    public void setDataService(ServiceDetailResponse serviceDetailResponse){
        mDataLocate.descripcion = serviceDetailResponse.listMensaje.get(0).descripcion;
        mDataLocate.id  = serviceDetailResponse.listMensaje.get(0).id;
        mDataLocate.detalle_id = "";
        mDataLocate.urlFoto = serviceDetailResponse.listMensaje.get(0).urlFoto;
        mDataLocate.periodo = serviceDetailResponse.listMensaje.get(0).periodo;
        mDataLocate.latitud = serviceDetailResponse.listMensaje.get(0).latitud;
        mDataLocate.esRutaTuristica = serviceDetailResponse.listMensaje.get(0).es_ruta_turistica;
        mDataLocate.longitud = serviceDetailResponse.listMensaje.get(0).longitud;
        mDataLocate.titulo = mTitle;
        detectLocation();
    }

    public void detectLocation(){
        try {
            if (mDataLocate.esRutaTuristica.equals("S")) {
                mLocationImageView.setVisibility(View.VISIBLE);
            } else {
                mLocationImageView.setVisibility(View.GONE);
            }
        }catch(NullPointerException e){
            e.printStackTrace();
            mLocationImageView.setVisibility(View.GONE);
        }
    }

    public String generateTypeNovelty(){
        if(mTypeNovelty.equals("N")){
            return "detalle-noticias";
        }else {
            return "detalle-promociones";
        }
    }

    public void getShareLink(String mTypeSection){

        switch (mTypeSection) {
            case "Servicios":
                mShareLink = "https://www.xochimilcoturismo.com/paginaxogo/detalle_servicios.html?id=" + mDataLocate.id;
            break;

            case "Novedades":
                mShareLink = "https://www.xochimilcoturismo.com/paginaxogo/" + generateTypeNovelty() + ".html?id=" + mDataLocate.id;
            break;
            default:
                mShareLink = "https://www.xochimilcoturismo.com/paginaxogov1/detalle-lugares-detalle.html";
                break;
        }
    }
}
