package com.firtsoft.xogo.Activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;

import com.firtsoft.xogo.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class EventDetailActivity extends AppCompatActivity {
    @BindView(R.id.toolbar_img_accept)
    ImageView imgBackToolbar;
    @BindView(R.id.toolbar_img_photo)
    ImageView imgPhotoToolbar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_detail);
        ButterKnife.bind(this);
        imgBackToolbar.setVisibility(View.GONE);
        imgPhotoToolbar.setVisibility(View.GONE);
    }

    @OnClick(R.id.toolbar_back)
    public void onClicBack(){
        finish();
    }
}
