package com.firtsoft.xogo.Fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.firtsoft.xogo.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProfileFragment extends Fragment {
    @BindView(R.id.toolbar_back)
    ImageView imgBackToolbar;
    @BindView(R.id.toolbar_img_photo)
    ImageView imgPhotoToolbar;
    public View rootView;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView =  inflater.inflate(R.layout.fragment_profile, container, false);
        return rootView;
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        ButterKnife.bind(this, rootView);
        imgBackToolbar.setVisibility(View.GONE);
        imgPhotoToolbar.setVisibility(View.GONE);
    }
}
