package com.firtsoft.xogo.Fragments;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.firtsoft.xogo.Activities.DetailActivity;
import com.firtsoft.xogo.Activities.ListsSecondLevelActivity;
import com.firtsoft.xogo.Activities.NewLocateActivity;
import com.firtsoft.xogo.Adapters.LocateAdapter;
import com.firtsoft.xogo.Adapters.LocateHolder;
import com.firtsoft.xogo.Background.ApiCient;
import com.firtsoft.xogo.Background.Request.LocateRequest;
import com.firtsoft.xogo.Background.Response.LocateResponse;
import com.firtsoft.xogo.Background.WebServices;
import com.firtsoft.xogo.InternalData.PrefsXogo;
import com.firtsoft.xogo.Model.DataLocate;
import com.firtsoft.xogo.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LocateFragment extends Fragment implements LocateAdapter.LocateCallback {
    @BindView(R.id.mListLocateRecyclerView)
    public RecyclerView mListLocateRecyclerView;
    @BindView(R.id.mSearchEditText)
    public EditText mSearchEditText;
    @BindView(R.id.fr_locate_tittle)
    public TextView fr_locate_tittle;

    public View rootView;
    private LocateAdapter mLocateAdapter;
    private List<DataLocate> mListLocate = new ArrayList<>();
    private PrefsXogo mPrefsXogo;
    private ProgressDialog pd;
    private String mLanguage;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView =  inflater.inflate(R.layout.fragment_locate, container, false);
        return rootView;
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        ButterKnife.bind(this, rootView);
        mListLocate.clear();
        configureRecycler();
        mPrefsXogo = new PrefsXogo(getActivity());
        pd = new ProgressDialog(getActivity());
        getLocate("","1","");
        addListener();
        setLanguage();
    }

    @OnClick(R.id.fr_locate_new_locate)
    public void onClicNewLocate(){
        startActivity(new Intent(getActivity(), NewLocateActivity.class));
    }


    public void setLanguage(){
        if(mPrefsXogo.getData("typeLanguage").equals("Spanish")){
            mLanguage= "Lugares";
            fr_locate_tittle.setText(mLanguage);
            mSearchEditText.setHint("Buscar");
        }else{
            mLanguage= "Places";
            fr_locate_tittle.setText(mLanguage);
            mSearchEditText.setHint("Search");
        }
    }

    private void configureRecycler(){
        mListLocateRecyclerView.setLayoutManager(new StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.VERTICAL));
        mLocateAdapter = new LocateAdapter(this);
        mListLocateRecyclerView.setAdapter(mLocateAdapter);
        mLocateAdapter.update(mListLocate);
    }

    public void getLocate(String cabecera_id, String level, String detalle_id){
        final WebServices res = ApiCient.getCliente().create(WebServices.class);
        LocateRequest locateRequest = new LocateRequest(mPrefsXogo.getData("typeLanguage").equals("English") ? "I" : "E","L",level,cabecera_id,detalle_id);
        Call<LocateResponse> call = res.get_locate(locateRequest);

        call.enqueue(new Callback<LocateResponse>() {
            @Override
            public void onResponse(Call<LocateResponse> call, Response<LocateResponse> response) {
                LocateResponse locateResponse = response.body();
                if(locateResponse.estatus.equals("ok")){
                    mListLocate = locateResponse.mensaje.data;
                    mLocateAdapter.update(mListLocate);
                }
            }

            @Override
            public void onFailure(Call<LocateResponse> call, Throwable t) {
                Log.e("Error","Error al tratar de establecer conexion con el servidor");
            }
        });
    }

    public void addListener(){
        mSearchEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String cad = "";
                cad = cad +s;
                searchInList(cad);
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }

    public void searchInList(String mParamSearch){
        List<DataLocate> mlistSearchResult = new ArrayList<>();
        for (DataLocate item: mListLocate) {
            if(item.titulo.toLowerCase().contains(mParamSearch.toLowerCase()) || item.titulo.toLowerCase().startsWith(mParamSearch.toLowerCase())){
                mlistSearchResult.add(item);
            }
        }
        mLocateAdapter.update(mlistSearchResult);
    }


    @Override
    public void OnSelectLocate(LocateHolder vh) {
        DataLocate dataLocate= vh.getLocate();
        isFather(dataLocate);
    }

    public void isFather(DataLocate mDataLocate){
        if(mDataLocate.esPadre.equals("S")){
            ListsSecondLevelActivity.launch(getActivity(),"2",mDataLocate.cabeceraId," ",mLanguage,"S");
        }else if(mDataLocate.esPadre.equals("N")){
            DetailActivity.launch(getActivity(),mDataLocate.descripcion,mDataLocate.urlFoto, "", mDataLocate.titulo, "", mDataLocate.nombre, mDataLocate,"false","");
        }
    }
}
