package com.firtsoft.xogo.Fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.firtsoft.xogo.Adapters.EventAdapter;
import com.firtsoft.xogo.Adapters.RouteAdapter;
import com.firtsoft.xogo.Adapters.RouteHolder;
import com.firtsoft.xogo.Model.RouteBean;
import com.firtsoft.xogo.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RouteFragment extends Fragment implements RouteAdapter.RouteCallback {
    @BindView(R.id.mRouteListRecyclerView)
    public RecyclerView mRouteListRecyclerView;
    public View rootView;
    private RouteAdapter mRouteAdapter;
    private List<RouteBean> mListRoute = new ArrayList<>();

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView =  inflater.inflate(R.layout.fragment_route, container, false);
        return rootView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this,view);
        mListRoute.clear();
        popuateRouteS();
        configureRecycler();
    }

    private void configureRecycler(){
        mRouteListRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRouteAdapter = new RouteAdapter(this);
        mRouteListRecyclerView.setAdapter(mRouteAdapter);
        mRouteAdapter.update(mListRoute);
    }

    private void popuateRouteS(){
        mListRoute.add(new RouteBean("https://agua.org.mx/wp-content/uploads/2013/05/Ajolote-xochimilco.png","Visita tu museo","11.10.2018","Museo dolores olmedo ocuoa que fuera del museo"));
        mListRoute.add(new RouteBean("https://agua.org.mx/wp-content/uploads/2013/05/Ajolote-xochimilco.png","La llorona","11.10.2018","Museo dolores olmedo ocuoa que fuera del museo"));
        mListRoute.add(new RouteBean("https://agua.org.mx/wp-content/uploads/2013/05/Ajolote-xochimilco.png","Paseos en el lago","11.10.2018","Museo dolores olmedo ocuoa que fuera del museo"));
        mListRoute.add(new RouteBean("https://agua.org.mx/wp-content/uploads/2013/05/Ajolote-xochimilco.png","Viajes gratis en diciembre","11.10.2018","Museo dolores olmedo ocuoa que fuera del museo"));
    }

    @Override
    public void OnSelectRoute(RouteHolder vh) {

    }
}
