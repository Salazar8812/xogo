package com.firtsoft.xogo.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.firtsoft.xogo.Activities.ConfirmChangeLenguageDialog;
import com.firtsoft.xogo.Activities.DetailActivity;
import com.firtsoft.xogo.Activities.FavsActivity;
import com.firtsoft.xogo.Activities.MainActivity;
import com.firtsoft.xogo.InternalData.PrefsXogo;
import com.firtsoft.xogo.R;
import com.firtsoft.xogo.Utils.Languages;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class FavoritiesFragment extends Fragment implements ConfirmChangeLenguageDialog.ChangeLanguageCallback{
    @BindView(R.id.fr_favorities_switch_spanish)
    TextView mLenguageSpanishTextView;
    @BindView(R.id.fr_favorities_switch_english)
    TextView mLenguageEnglishTextView;
    private PrefsXogo prefsXogo;
    public View rootView;

    @BindView(R.id.mFavsButton)
    public Button mFavsButton;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView =  inflater.inflate(R.layout.fragment_favorities, container, false);
        return rootView;
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        ButterKnife.bind(this, rootView);
        prefsXogo = new PrefsXogo(getActivity());
        setLanguage();
    }

    public void setLanguage(){
        if(prefsXogo.getData("typeLanguage").equals("Spanish")){
            mFavsButton.setText("Favoritos");
            mLenguageSpanishTextView.setText("Español");
            mLenguageEnglishTextView.setText("Inglés");
            selectLanguageSpanish();
        }else{
            mFavsButton.setText("Favorites");
            mLenguageSpanishTextView.setText("Spanish");
            mLenguageEnglishTextView.setText("English");
            selectLanguageEnglish();
        }
    }

    public void selectLanguageSpanish(){
        mLenguageSpanishTextView.setBackgroundResource(R.drawable.background_btn_type_session_left_r);
        mLenguageSpanishTextView.setTextColor(getResources().getColor(R.color.white));
        mLenguageEnglishTextView.setTextColor(getResources().getColor(R.color.red));
        mLenguageEnglishTextView.setBackgroundResource(R.drawable.background_btn_type_session_right2_r);
    }

    public void selectLanguageEnglish(){
        mLenguageEnglishTextView.setBackgroundResource(R.drawable.background_btn_type_session_right_r);
        mLenguageEnglishTextView.setTextColor(getResources().getColor(R.color.white));
        mLenguageSpanishTextView.setTextColor(getResources().getColor(R.color.red));
        mLenguageSpanishTextView.setBackgroundResource(R.drawable.background_btn_type_session_left2_r);
    }

    public void setEnglisLanguage(){
        mLenguageSpanishTextView.setText(Languages.languageEN[0]);
        mLenguageEnglishTextView.setText(Languages.languageEN[1]);

        startActivity(new Intent(getActivity(), MainActivity.class));
        prefsXogo.saveData("typeLanguage","English");
    }

    public void setSpanishLanguage(){
        mLenguageSpanishTextView.setText(Languages.languageES[0]);
        mLenguageEnglishTextView.setText(Languages.languageES[1]);
        startActivity(new Intent(getActivity(), MainActivity.class));

        prefsXogo.saveData("typeLanguage","Spanish");
    }

    @OnClick(R.id.mFavsButton)
    public void OnClickFavs(){
        FavsActivity.launch(getActivity());
    }

    @OnClick(R.id.fr_favorities_switch_spanish)
    public void onClickSpanish(){
        selectLanguageSpanish();
        setSpanishLanguage();
        /*ConfirmChangeLenguageDialog confirmChangeLenguageDialog = new ConfirmChangeLenguageDialog();
        confirmChangeLenguageDialog.getCallback(this);
        startActivity(confirmChangeLenguageDialog.launch(getActivity(),"Spanish"));*/
    }

    @OnClick(R.id.fr_favorities_switch_english)
    public void onClickEnglish(){
        selectLanguageEnglish();
        setEnglisLanguage();
        /*ConfirmChangeLenguageDialog confirmChangeLenguageDialog = new ConfirmChangeLenguageDialog();
        confirmChangeLenguageDialog.getCallback(this);
        startActivity(confirmChangeLenguageDialog.launch(getActivity(),"English"));*/
    }

    @Override
    public void OnSetEnglishLanguage(String mTypeLanguage){
        if(mTypeLanguage.equals("Spanish")){
            setSpanishLanguage();
        }else{
            setEnglisLanguage();
        }
    }
}
