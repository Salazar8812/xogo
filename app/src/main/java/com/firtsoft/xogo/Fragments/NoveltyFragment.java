package com.firtsoft.xogo.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.firtsoft.xogo.Activities.DetailActivity;
import com.firtsoft.xogo.Activities.ListsSecondLevelActivity;
import com.firtsoft.xogo.Activities.NewEventActivity;
import com.firtsoft.xogo.Adapters.EventAdapter;
import com.firtsoft.xogo.Adapters.EventHolder;
import com.firtsoft.xogo.Background.ApiCient;
import com.firtsoft.xogo.Background.Request.LocateRequest;
import com.firtsoft.xogo.Background.Response.LocateResponse;
import com.firtsoft.xogo.Background.WebServices;
import com.firtsoft.xogo.InternalData.PrefsXogo;
import com.firtsoft.xogo.Model.DataLocate;
import com.firtsoft.xogo.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NoveltyFragment extends Fragment implements EventAdapter.EventCallback {

    @BindView(R.id.mListEventRecyclerViewNovelty)
    public RecyclerView mListEventRecyclerView;
    public View rootView;
    @BindView(R.id.fr_favorities_switch_spanish)
    TextView mLenguageSpanishTextView;
    @BindView(R.id.fr_favorities_switch_english)
    TextView mLenguageEnglishTextView;

    @BindView(R.id.fr_novelty_tittle)
    TextView fr_novelty_tittle;

    @BindView(R.id.mSearchEditText)
    public EditText mSearchEditText;
    private EventAdapter mEventAdapter;
    private List<DataLocate> mListEvent = new ArrayList<>();
    private PrefsXogo mPrefsXogo;
    private String typeNovelty = "N";
    private String mLangugeTitle;


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView =  inflater.inflate(R.layout.fragment_novelty, container, false);
        return rootView;
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        ButterKnife.bind(this, rootView);
        mListEvent.clear();
        configureRecycler();
        mPrefsXogo = new PrefsXogo(getActivity());
        getNovelty();
        selectNews();
        addListener();
        setLanguage();
    }

    public void setLanguage(){
        if(mPrefsXogo.getData("typeLanguage").equals("Spanish")){
            mLangugeTitle = "Novedades";
            fr_novelty_tittle.setText(mLangugeTitle);
            mSearchEditText.setHint("Buscar");
            mLenguageSpanishTextView.setText("Noticias");
            mLenguageEnglishTextView.setText("Promociones");
        }else{
            mLangugeTitle = "News";
            fr_novelty_tittle.setText(mLangugeTitle);
            mLangugeTitle = "Novedades";
            mSearchEditText.setHint("Search");
            mLenguageSpanishTextView.setText("News");
            mLenguageEnglishTextView.setText("Promotions");
        }
    }

    @OnClick(R.id.fr_events_new_event)
    public void onClicNewLocate(){
        startActivity(new Intent(getActivity(), NewEventActivity.class));
    }

    private void configureRecycler(){
        mListEventRecyclerView.setLayoutManager(new StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.VERTICAL));
        mEventAdapter = new EventAdapter(this);
        mListEventRecyclerView.setAdapter(mEventAdapter);
        mEventAdapter.update(mListEvent);
    }

    public void getNovelty(){
        final WebServices res = ApiCient.getCliente().create(WebServices.class);
        LocateRequest locateRequest = new LocateRequest(mPrefsXogo.getData("typeLanguage").equals("English") ? "I" : "E",typeNovelty,"","");
        Call<LocateResponse> call = res.get_novelty(locateRequest);

        call.enqueue(new Callback<LocateResponse>() {
            @Override
            public void onResponse(Call<LocateResponse> call, Response<LocateResponse> response) {
                LocateResponse locateResponse = response.body();
                if(locateResponse.estatus.equals("ok")){
                    mListEvent = locateResponse.mensaje.data;
                    mEventAdapter.update(locateResponse.mensaje.data);
                }
            }
            @Override
            public void onFailure(Call<LocateResponse> call, Throwable t) {
                Log.e("Error","Error al tratar de establecer conexion con el servidor");
            }
        });
    }


    @Override
    public void OnSelectEvent(EventHolder vh) {
        DataLocate dataLocate= vh.getEvent();
        isFather(dataLocate);
    }

    public void isFather(DataLocate mDataLocate){
        DetailActivity.launch(getActivity(), mDataLocate.descripcion, mDataLocate.urlFoto, "", mDataLocate.nombre, "", mDataLocate.titulo,mDataLocate,mLangugeTitle,typeNovelty);
    }

    public void addListener(){
        mSearchEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String cad = "";
                cad = cad +s;
                searchInList(cad);
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }

    public void searchInList(String mParamSearch){
        List<DataLocate> mlistSearchResult = new ArrayList<>();
        for (DataLocate item: mListEvent) {
            if(item.nombre.toLowerCase().contains(mParamSearch.toLowerCase()) || item.nombre.toLowerCase().startsWith(mParamSearch.toLowerCase())){
                mlistSearchResult.add(item);
            }
        }
        mEventAdapter.update(mlistSearchResult);
    }

    public void selectNews(){
        typeNovelty = "N";
        mLenguageSpanishTextView.setBackgroundResource(R.drawable.background_btn_type_session_left_r);
        mLenguageSpanishTextView.setTextColor(getResources().getColor(R.color.white));
        mLenguageEnglishTextView.setTextColor(getResources().getColor(R.color.red));
        mLenguageEnglishTextView.setBackgroundResource(R.drawable.background_btn_type_session_right2_r);
        getNovelty();
    }

    @OnClick(R.id.fr_favorities_switch_spanish)
    public void onClickNoticias(){
       selectNews();
    }

    @OnClick(R.id.fr_favorities_switch_english)
    public void onClickNovedades(){
        typeNovelty = "P";
        mLenguageEnglishTextView.setBackgroundResource(R.drawable.background_btn_type_session_right_r);
        mLenguageEnglishTextView.setTextColor(getResources().getColor(R.color.white));
        mLenguageSpanishTextView.setTextColor(getResources().getColor(R.color.red));
        mLenguageSpanishTextView.setBackgroundResource(R.drawable.background_btn_type_session_left2_r);
        getNovelty();
    }

}
