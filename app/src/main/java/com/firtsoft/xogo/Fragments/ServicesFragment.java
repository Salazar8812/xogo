package com.firtsoft.xogo.Fragments;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.firtsoft.xogo.Activities.DetailActivity;
import com.firtsoft.xogo.Activities.NewEventActivity;
import com.firtsoft.xogo.Adapters.EventAdapter;
import com.firtsoft.xogo.Adapters.EventHolder;
import com.firtsoft.xogo.Background.ApiCient;
import com.firtsoft.xogo.Background.Request.LocateRequest;
import com.firtsoft.xogo.Background.Response.LocateResponse;
import com.firtsoft.xogo.Background.WebServices;
import com.firtsoft.xogo.InternalData.PrefsXogo;
import com.firtsoft.xogo.Model.DataLocate;
import com.firtsoft.xogo.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ServicesFragment extends Fragment implements EventAdapter.EventCallback  {

    @BindView(R.id.mListEventRecyclerViewServices)
    public RecyclerView mListEventRecyclerView;

    @BindView(R.id.mSearchEditText)
    public EditText mSearchEditText;

    @BindView(R.id.fr_services_tittle)
    public TextView fr_services_tittle;
    public View rootView;
    private EventAdapter mEventAdapter;
    private List<DataLocate> mListEvent = new ArrayList<>();
    private ProgressDialog pd;
    private PrefsXogo mPrefsXogo;
    private String mLanguage = "";

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView =  inflater.inflate(R.layout.fragment_services, container, false);
        return rootView;
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        ButterKnife.bind(this, rootView);
        mListEvent.clear();
        configureRecycler();
        mPrefsXogo = new PrefsXogo(getActivity());
        getService();
        addListener();
        setLanguage();
    }

    @OnClick(R.id.fr_events_new_event)
    public void onClicNewLocate(){
        startActivity(new Intent(getActivity(), NewEventActivity.class));
    }

    public void setLanguage(){
        if(mPrefsXogo.getData("typeLanguage").equals("Spanish")){
            mLanguage= "Servicios";
            fr_services_tittle.setText(mLanguage);
            mSearchEditText.setHint("Buscar");
        }else{
            mLanguage= "Services";
            fr_services_tittle.setText(mLanguage);
            mLanguage= "Servicios";
            mSearchEditText.setHint("Search");
        }
    }

    private void configureRecycler(){
        mListEventRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mEventAdapter = new EventAdapter(this);
        mListEventRecyclerView.setAdapter(mEventAdapter);
        mEventAdapter.update(mListEvent);
    }

    public void getService(){
        final WebServices res = ApiCient.getCliente().create(WebServices.class);
        Call<LocateResponse> call = res.get_service(new LocateRequest(mPrefsXogo.getData("typeLanguage").equals("English") ? "E":"S","","","",""));

        call.enqueue(new Callback<LocateResponse>() {
            @Override
            public void onResponse(Call<LocateResponse> call, Response<LocateResponse> response) {
                LocateResponse locateResponse = response.body();
                if(locateResponse.estatus.equals("ok")){
                    mListEvent = locateResponse.mensaje.data;
                    mEventAdapter.update(locateResponse.mensaje.data);
                }
            }
            @Override
            public void onFailure(Call<LocateResponse> call, Throwable t) {
                Log.e("Error",t.toString());
            }
        });
    }


    public void addListener(){
        mSearchEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String cad = "";
                cad = cad +s;
                searchInList(cad);
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }

    public void searchInList(String mParamSearch){
        List<DataLocate> mlistSearchResult = new ArrayList<>();
        for (DataLocate item: mListEvent) {
            if(item.nombre.toLowerCase().contains(mParamSearch.toLowerCase()) || item.nombre.toLowerCase().startsWith(mParamSearch.toLowerCase())){
                mlistSearchResult.add(item);
            }
        }
        mEventAdapter.update(mlistSearchResult);
    }


    @Override
    public void OnSelectEvent(EventHolder vh) {
        DataLocate dataLocate= vh.getEvent();
        isFather(dataLocate);
    }

    public void isFather(DataLocate mDataLocate){
        DetailActivity.launch(getActivity(), mDataLocate.descripcion, mDataLocate.urlFoto, "", mDataLocate.nombre, "", mDataLocate.titulo,mDataLocate,mLanguage,"");
    }
}
