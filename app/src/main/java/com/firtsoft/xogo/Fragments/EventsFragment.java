package com.firtsoft.xogo.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.firtsoft.xogo.Adapters.EventAdapter;
import com.firtsoft.xogo.Adapters.EventHolder;
import com.firtsoft.xogo.Model.DataLocate;
import com.firtsoft.xogo.Model.EventBean;
import com.firtsoft.xogo.Activities.NewEventActivity;
import com.firtsoft.xogo.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class EventsFragment extends Fragment implements EventAdapter.EventCallback {
    @BindView(R.id.mListEventRecyclerView)
    public RecyclerView mListEventRecyclerView;
    public View rootView;
    private EventAdapter mEventAdapter;
    private List<DataLocate> mListEvent = new ArrayList<>();

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView =  inflater.inflate(R.layout.fragment_events, container, false);
        return rootView;
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        ButterKnife.bind(this, rootView);
        mListEvent.clear();
        populateEventTemp();
        configureRecycler();
    }

    @OnClick(R.id.fr_events_new_event)
    public void onClicNewLocate(){
        startActivity(new Intent(getActivity(), NewEventActivity.class));
    }

    private void configureRecycler(){
        mListEventRecyclerView.setLayoutManager(new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL));
        mEventAdapter = new EventAdapter(this);
        mListEventRecyclerView.setAdapter(mEventAdapter);
        mEventAdapter.update(mListEvent);
    }

    private void populateEventTemp(){
        /*mListEvent.add(new EventBean("https://agua.org.mx/wp-content/uploads/2013/05/Ajolote-xochimilco.png","Venta de flores reginoales Xochimilco","30 de noviembre 2018","Costo: 100.00"));
        mListEvent.add(new EventBean("https://previews.123rf.com/images/mardzpe/mardzpe1410/mardzpe141000089/32681418-colourful-mexican-gondolas-at-xochimilco.jpg","Venta de flores reginoales Xochimilco","30 de noviembre 2018","Costo: 100.00"));
        mListEvent.add(new EventBean("http://www.caniem.com/sites/default/files/styles/product_470x470/public/portadas/Portada%20El%20ajolote%20de%20Xochimilco.jpg?itok=HUhg37aL","Venta de flores reginoales Xochimilco","30 de noviembre 2018","Costo: 100.00"));
        mListEvent.add(new EventBean("https://previews.123rf.com/images/mardzpe/mardzpe1410/mardzpe141000087/32681416-colourful-mexican-gondolas-at-xochimilco.jpg","Venta de flores reginoales Xochimilco","30 de noviembre 2018","Costo: 100.00"));
        mListEvent.add(new EventBean("https://previews.123rf.com/images/rafaelbenari/rafaelbenari1510/rafaelbenari151005759/46309296-mexico-city-february-28-colorful-mexican-gondolas-at-xochimilcos-floating-gardens-on-febuary-28-2010.jpg","Venta de flores reginoales Xochimilco","30 de noviembre 2018","Costo: 100.00"));
        mListEvent.add(new EventBean("https://previews.123rf.com/images/diegograndi/diegograndi1704/diegograndi170400410/75600102-mexican-colorful-boats-at-xochimilco-s-floating-gardens-mexico-city-mexico.jpg","Venta de flores reginoales Xochimilco","30 de noviembre 2018","Costo: 100.00"));
        mListEvent.add(new EventBean("https://commons.wikimedia.org/wiki/File:Canales_de_xochimilco.jpg","Venta de flores reginoales Xochimilco","30 de noviembre 2018","Costo: 100.00"));
        mListEvent.add(new EventBean("https://previews.123rf.com/images/diegograndi/diegograndi1706/diegograndi170600524/82224631-colorful-boat-also-known-as-trajinera-at-xochimilco-s-floating-gardens-mexico-city-mexico.jpg","Venta de flores reginoales Xochimilco","30 de noviembre 2018","Costo: 100.00"));
        mListEvent.add(new EventBean("https://commons.wikimedia.org/wiki/File:Lago_Xochimilco.jpg","Venta de flores reginoales Xochimilco","30 de noviembre 2018","Costo: 100.00"));
        mListEvent.add(new EventBean("https://previews.123rf.com/images/mardzpe/mardzpe1410/mardzpe141000087/32681416-colourful-mexican-gondolas-at-xochimilco.jpg","Venta de flores reginoales Xochimilco","30 de noviembre 2018","Costo: 100.00"));
        mListEvent.add(new EventBean("https://previews.123rf.com/images/diegograndi/diegograndi1706/diegograndi170600524/82224631-colorful-boat-also-known-as-trajinera-at-xochimilco-s-floating-gardens-mexico-city-mexico.jpg","Venta de flores reginoales Xochimilco","30 de noviembre 2018","Costo: 100.00"));
    */
    }

    @Override
    public void OnSelectEvent(EventHolder vh) {

    }
}
