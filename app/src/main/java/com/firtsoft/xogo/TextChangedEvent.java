package com.firtsoft.xogo;

import android.support.v4.app.Fragment;

/**
 * Created by imacbookpro on 10/2/18.
 * TPEmpresarial
 */

public class TextChangedEvent {
    public int position;
    public Fragment mFragment;
    public boolean isReplace;

    public TextChangedEvent(int position, Fragment fragment, boolean isReplace) {
        this.position = position;
        mFragment = fragment;
        this.isReplace =  isReplace;
    }
}
