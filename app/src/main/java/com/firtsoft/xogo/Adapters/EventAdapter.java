package com.firtsoft.xogo.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.firtsoft.xogo.Model.DataLocate;
import com.firtsoft.xogo.R;
import java.util.ArrayList;
import java.util.List;

public class EventAdapter extends RecyclerView.Adapter<EventHolder> {
    private List<DataLocate> mListEvent = new ArrayList<>();
    private List<EventHolder> mViewHolders = new ArrayList<>();
    private EventCallback mEventCallback;

    public EventAdapter(EventCallback mEventCallback) {
        this.mEventCallback = mEventCallback;
    }

    @NonNull
    @Override
    public EventHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return insertViewHolder(new EventHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_locate,
                viewGroup, false)));
    }

    private EventHolder insertViewHolder(@NonNull EventHolder vh) {
        if (!mViewHolders.contains(vh)) {
            mViewHolders.add(vh);
        }
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull EventHolder eventHolder, int i) {
        eventHolder.render(mListEvent.get(i),mEventCallback);
    }

    @Override
    public int getItemCount() {
        return mListEvent.size();
    }

    public void update(List<DataLocate> mListEvent){
        this.mListEvent = mListEvent;
        notifyDataSetChanged();
    }

    public interface EventCallback{
        void OnSelectEvent(EventHolder vh);
    }
}
