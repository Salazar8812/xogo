package com.firtsoft.xogo.Adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.firtsoft.xogo.Model.DataLocate;
import com.firtsoft.xogo.R;

public class LocateHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    private ImageView mLocateImageViewImageView;
    private TextView mTitleLocateTextView;
    private DataLocate mLocateBean;
    private LocateAdapter.LocateCallback mLocateCallback;

    public LocateHolder(@NonNull View itemView) {
        super(itemView);
        mLocateImageViewImageView = itemView.findViewById(R.id.mLocateImageViewImageView);
        mTitleLocateTextView = itemView.findViewById(R.id.mTitleLocateTextView);
    }
    public void render(@NonNull DataLocate mLocateBean , LocateAdapter.LocateCallback mLocateCallback) {
        this.mLocateBean = mLocateBean;
        mTitleLocateTextView.setText(mLocateBean.titulo);
        RequestOptions requestOption = new RequestOptions()
                .placeholder(R.drawable.ic_placeholder).centerCrop();
        Glide.with(itemView.getContext()).load(mLocateBean.urlFoto).apply(requestOption).into(mLocateImageViewImageView);
        this.mLocateCallback = mLocateCallback;
        itemView.setOnClickListener(this);
    }

    public DataLocate getLocate(){
        return mLocateBean;
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == itemView.getId()){
            mLocateCallback.OnSelectLocate(this);
        }
    }
}
