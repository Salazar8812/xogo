package com.firtsoft.xogo.Adapters;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.firtsoft.xogo.Model.DataLocate;
import com.firtsoft.xogo.R;

import java.util.ArrayList;
import java.util.List;

public class LocateAdapter extends RecyclerView.Adapter<LocateHolder>  {
    private List<DataLocate> mListLocate = new ArrayList<>();
    private List<LocateHolder> mViewHolders = new ArrayList<>();
    private AppCompatActivity mAppCompatActivity;
    private LocateCallback mLocateCallback;

    public LocateAdapter(LocateCallback mLocateCallback) {
        this.mLocateCallback =mLocateCallback;
    }

    @NonNull
    @Override
    public LocateHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return insertViewHolder(new LocateHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_locate,
                viewGroup, false)));
    }

    private LocateHolder insertViewHolder(@NonNull LocateHolder vh) {
        if (!mViewHolders.contains(vh)) {
            mViewHolders.add(vh);
        }
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull LocateHolder eventHolder, int i) {
        eventHolder.render(mListLocate.get(i), mLocateCallback);
    }

    @Override
    public int getItemCount() {
        return mListLocate.size();
    }

    public void update(List<DataLocate> mListLocate){
        this.mListLocate = mListLocate;
        notifyDataSetChanged();
    }

    public interface LocateCallback{
        void OnSelectLocate(LocateHolder vh);
    }
}
