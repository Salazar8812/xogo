package com.firtsoft.xogo.Adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.firtsoft.xogo.Model.DataLocate;
import com.firtsoft.xogo.R;

public class EventHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    private ImageView mEventImageView;
    private TextView mTitleEventTextView;
    private TextView mDateEventTextView;
    private TextView mPriceEventTextView;
    private DataLocate mEventBean;
    private EventAdapter.EventCallback mEventCallback;

    public EventHolder(@NonNull View itemView) {
        super(itemView);
        mEventImageView = itemView.findViewById(R.id.mLocateImageViewImageView);
        mTitleEventTextView = itemView.findViewById(R.id.mTitleLocateTextView);
        itemView.setOnClickListener(this);
    }

    public void render(@NonNull DataLocate mEventBean, EventAdapter.EventCallback mEventCallback) {
        this.mEventBean = mEventBean;
        mTitleEventTextView.setText(mEventBean.nombre);
        RequestOptions requestOption = new RequestOptions()
                .placeholder(R.drawable.ic_placeholder).centerCrop();
        Glide.with(itemView.getContext()).load(mEventBean.urlFoto).apply(requestOption).into(mEventImageView);
        this.mEventCallback = mEventCallback;
        itemView.setOnClickListener(this);

    }

    public DataLocate getEvent(){
        return mEventBean;
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == itemView.getId()){
            mEventCallback.OnSelectEvent(this);
        }
    }
}
