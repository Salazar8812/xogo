package com.firtsoft.xogo.Adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.firtsoft.xogo.Model.DataLocate;
import com.firtsoft.xogo.R;

public class FavsHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    private ImageView mFavsImageView;
    private TextView mTitleFavsTextView;
    private DataLocate mLocateBean;
    private FavsAdapter.LocateCallback mLocateCallback;

    public FavsHolder(@NonNull View itemView) {
        super(itemView);
        mFavsImageView = itemView.findViewById(R.id.mFavsImageView);
        mTitleFavsTextView = itemView.findViewById(R.id.mTitleFavsTextView);
    }
    public void render(@NonNull DataLocate mLocateBean , FavsAdapter.LocateCallback mLocateCallback) {
        this.mLocateBean = mLocateBean;
        mTitleFavsTextView.setText(mLocateBean.titulo);
        RequestOptions requestOption = new RequestOptions()
                .placeholder(R.drawable.ic_placeholder).centerCrop();
        Glide.with(itemView.getContext()).load(mLocateBean.urlFoto).apply(requestOption).into(mFavsImageView);
        this.mLocateCallback = mLocateCallback;
        itemView.setOnClickListener(this);
    }

    public DataLocate getLocate(){
        return mLocateBean;
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == itemView.getId()){
            mLocateCallback.OnSelectLocate(this);
        }
    }
}
