package com.firtsoft.xogo.Adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import com.firtsoft.xogo.Model.RouteBean;
import com.firtsoft.xogo.R;
import java.util.ArrayList;
import java.util.List;

public class RouteAdapter extends RecyclerView.Adapter<RouteHolder>  {
    private RouteCallback mRouteCallback;
    private List<RouteBean> mListRoute = new ArrayList<>();
    private List<RouteHolder> mViewHolders = new ArrayList<>();

    public RouteAdapter(RouteCallback mRouteCallback) {
        this.mRouteCallback = mRouteCallback;
    }

    @NonNull
    @Override
    public RouteHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return insertViewHolder(new RouteHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_route,
                viewGroup, false)));
    }

    private RouteHolder insertViewHolder(@NonNull RouteHolder vh) {
        if (!mViewHolders.contains(vh)) {
            mViewHolders.add(vh);
        }
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull RouteHolder eventHolder, int i) {
        eventHolder.render(mListRoute.get(i), mRouteCallback);
    }


    @Override
    public int getItemCount() {
        return mListRoute.size();
    }

    public interface RouteCallback{
        void OnSelectRoute(RouteHolder vh);
    }

    public void update(List<RouteBean> mListRoute){
        this.mListRoute = mListRoute;
        notifyDataSetChanged();
    }
}
