package com.firtsoft.xogo.Adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.firtsoft.xogo.Model.RouteBean;
import com.firtsoft.xogo.R;
import com.firtsoft.xogo.Utils.CircleImageView;

public class RouteHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    private CircleImageView mRouteImageView;
    private TextView mTitleRouteTextView;
    private TextView mDescriptionTextView;
    private TextView mDateTextView;

    private RouteBean mRouteBEan;
    private RouteAdapter.RouteCallback mRouteCallback;

    public RouteHolder(@NonNull View itemView) {
        super(itemView);
        mRouteImageView = itemView.findViewById(R.id.mRouteImageView);
        mTitleRouteTextView = itemView.findViewById(R.id.mTitleRouteTextView);
        mDescriptionTextView = itemView.findViewById(R.id.mDescriptionRouteTextView);
        mDateTextView = itemView.findViewById(R.id.mDateTextView);
        itemView.setOnClickListener(this);
    }

    public void render(@NonNull RouteBean mRouteBean , RouteAdapter.RouteCallback mRouteCallback) {
        this.mRouteBEan = mRouteBean;
        mTitleRouteTextView.setText(mRouteBean.mTitleRoute);
        mDescriptionTextView.setText(mRouteBean.mDescription);
        mDateTextView.setText(mRouteBean.mDateRoute);
        RequestOptions requestOption = new RequestOptions()
                .placeholder(R.drawable.ic_placeholder).centerCrop();
        Glide.with(itemView.getContext()).load(mRouteBean.mImageRoute).apply(requestOption).into(mRouteImageView);
        this.mRouteCallback = mRouteCallback;
        itemView.setOnClickListener(this);
    }

    public RouteBean getLocate(){
        return mRouteBEan;
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == itemView.getId()){
            mRouteCallback.OnSelectRoute(this);
        }
    }
}
