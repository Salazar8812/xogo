
package com.firtsoft.xogo.Model;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MessageLocate {

    @SerializedName("total")
    @Expose
    public String total;
    @SerializedName("per_page")
    @Expose
    public String perPage;
    @SerializedName("current_page")
    @Expose
    public String currentPage;
    @SerializedName("last_page")
    @Expose
    public String lastPage;
    @SerializedName("next_page_url")
    @Expose
    public String nextPageUrl;
    @SerializedName("prev_page_url")
    @Expose
    public Object prevPageUrl;
    @SerializedName("from")
    @Expose
    public String from;
    @SerializedName("to")
    @Expose
    public String to;
    @SerializedName("data")
    @Expose
    public List<DataLocate> data = new ArrayList<>();

    @SerializedName("url_foto")
    @Expose
    public String urlFoto;
    @SerializedName("descripcion")
    @Expose
    public String descripcion;

    @SerializedName("id")
    @Expose
    public String id;
    @SerializedName("nombre")
    @Expose
    public String nombre;
    @SerializedName("periodo")
    @Expose
    public String periodo;
    @SerializedName("latitud")
    @Expose
    public String latitud;
    @SerializedName("longitud")
    @Expose
    public String longitud;

    @SerializedName("codigo_postal")
    @Expose
    public String codigo_postal;

    @SerializedName("colonia")
    @Expose
    public String colonia;

    @SerializedName("calle")
    @Expose
    public String calle;

    @SerializedName("es_ruta_turistica")
    @Expose
    public String es_ruta_turistica;

}

