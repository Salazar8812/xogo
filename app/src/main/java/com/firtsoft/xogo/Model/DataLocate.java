
package com.firtsoft.xogo.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class DataLocate implements Serializable {


    @SerializedName("id")
    @Expose
    public String id;
    @SerializedName("detalle_id")
    @Expose
    public String detalle_id;
    @SerializedName("nombre")
    @Expose
    public String nombre;
    @SerializedName("periodo")
    @Expose
    public String periodo;
    @SerializedName("cabecera_id")
    @Expose
    public String cabeceraId;
    @SerializedName("es_padre")
    @Expose
    public String esPadre;
    @SerializedName("es_ruta_turistica")
    @Expose
    public String esRutaTuristica;
    @SerializedName("titulo")
    @Expose
    public String titulo;
    @SerializedName("url_foto")
    @Expose
    public String urlFoto;
    @SerializedName("descripcion")
    @Expose
    public String descripcion;
    @SerializedName("direccion")
    @Expose
    public String direccion;
    @SerializedName("latitud")
    @Expose
    public String latitud;
    @SerializedName("longitud")
    @Expose
    public String longitud;

}
