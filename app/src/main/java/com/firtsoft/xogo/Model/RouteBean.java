package com.firtsoft.xogo.Model;

public class RouteBean {
    public String mImageRoute;
    public String mTitleRoute;
    public String mDateRoute;
    public String mDescription;

    public RouteBean(String mImageRoute, String mTitleRoute, String mDateRoute, String mDescription) {
        this.mImageRoute = mImageRoute;
        this.mTitleRoute = mTitleRoute;
        this.mDateRoute = mDateRoute;
        this.mDescription = mDescription;
    }
}
