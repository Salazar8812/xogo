package com.firtsoft.xogo.Model;

public class EventBean {
    public String mImageEvent;
    public String mTitleEvent;
    public String mDateEvent;
    public String mPriceEvent;

    public EventBean() {
    }

    public EventBean(String mImageEvent, String mTitleEvent, String mDateEvent, String mPriceEvent) {
        this.mImageEvent = mImageEvent;
        this.mTitleEvent = mTitleEvent;
        this.mDateEvent = mDateEvent;
        this.mPriceEvent = mPriceEvent;
    }
}
