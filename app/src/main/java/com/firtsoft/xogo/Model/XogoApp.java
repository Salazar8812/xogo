package com.firtsoft.xogo.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class XogoApp {
    @SerializedName("idVersion")
    @Expose
    public String idVersion;
    @SerializedName("blockApp")
    @Expose
    public String blockApp;
    @SerializedName("name")
    @Expose
    public String name;
}
